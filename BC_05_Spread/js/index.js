// Dùng spread
// Tạo hiệu ứng hover từng chữ

// gọi biến

let heading = document.querySelector('.heading').innerText;
let headingSpread = [...heading];

// DOM và tạo hiệu ứng 
document.querySelector('.heading').innerText = '';
headingSpread.forEach((element) => {
    var contentText = 
    `
    <span>${element}</span>
    `;
    return document.querySelector('.heading').innerHTML += contentText;
});