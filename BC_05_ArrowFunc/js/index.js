// Tạo biến mảng màu
const colorList = ['pallet','viridian','pewter','cerulean','vermillion','lavender','celadon','saffron','fuschia','cinnabar'];

// Tạo bảng màu cho nhà 
let addColorButton = (array) => {
    let contentHTML ='';
    for (var i = 0; i < array.length; i++){
        contentButton = `
        <button class="color-button ${array[i]}"></button>
        `;
        contentHTML += contentButton;
    }
    document.getElementById('colorContainer').innerHTML = contentHTML;
};
addColorButton(colorList);

// tạo function hiện mũi tên và click làm đổi màu nhà

// Dùng tính năng set parent sibling 
const setActive = (element) => {
    [...element.parentElement.children].forEach((sibling) =>
    sibling.classList.remove("active")
    );
    element.classList.add("active");
};

// tạo mũi tên hover nút màu và biết mất sau khi không chọn nữa

let colorButtons = [...document.querySelectorAll(".color-button")];
let houseObject = document.getElementById('house');
console.log('colorButtons :', colorButtons);

colorButtons.forEach((button) => {
    // find the position of colro code in each button class (0): color-button (1): color code (2): active toggle
    let buttonColorPos = button.classList.item(1);

    console.log('buttonColorPos :', buttonColorPos);
    button.addEventListener("click", () => {
      setActive(button);
    // set color of current button to house
      houseObject.className = `house ${buttonColorPos}`
    });
});

